import { Button } from "./Components/Control.js";
import { Modal } from "./Components/Modal.js";
import "./Components/global.scss";
import React from "react";

class App extends React.Component {
  state = { openModalFirst: false, openModalSecond: false };

  render() {
    return (
      <div className="app">
        <Button
          className="btn"
          text="Open first modal"
          backgroundColor="red"
          handleClick={() => this.setState({ openModalFirst: true })}
        />
        <Button
          className="btn"
          text="Open second modal"
          backgroundColor="blue"
          handleClick={() => this.setState({ openModalSecond: true })}
        />
        {this.state.openModalFirst && (
          <Modal
            className="modal-first"
            header="First Modal"
            text="first"
            closeButton={() => this.setState({ openModalFirst: false })}
            actions={
              <>
                <Button className="modal-btn modal-btn-first" text="Ok" />
                <Button className="modal-btn modal-btn-first" text="Cancel" />
              </>
            }
          />
        )}
        {this.state.openModalSecond && (
          <Modal
            className="modal-second"
            header="Second Modal"
            text="second"
            closeButton={() => this.setState({ openModalSecond: false })}
            actions={
              <>
                <Button className="modal-btn modal-btn-second" text="Ok" />
                <Button className="modal-btn modal-btn-second" text="Cancel" />
              </>
            }
          />
        )}
      </div>
    );
  }
}
export default App;
